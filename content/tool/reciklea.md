---
title: "Mapa de reciclatge de mobles"
cover: "img/reciklea.svg"
description: "Mapa de reciclatge de mobles a barcelona"

---

**_Millor que l'IKEA, Recicla! :D_**

Mapa amb els dies de llençar i recollir mobles i trastos útils del carrer a la ciutat de Barcelona (excepte Vallvidrera).

Els carrers frontera són aproximats i es poden comprovar a https://www.barcelona.cat/cuidembarcelona/ca/reciclar/res/RM0093

L'horari de llençar els trastos és de 20h a 22h. Els camions de l'ajuntament comencen a passar a les 22h i a les 23h ja casi han acabat.


<div style="height: 100vh; width: 100%; margin: auto;">
    <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.instamaps.cat/visor.html?businessid=922054e29e8d3f0ff4b9d06c099d03bf&3D=false&embed=1" >
    </iframe>
</div>

[Mapa a pantalla completa](https://www.instamaps.cat/instavisor/922054e29e8d3f0ff4b9d06c099d03bf/Reciklea_BCN.html)


## Instaŀlar en el mòbil

A part de consultar el mapa per la web, les capes del mapa es poden importar al mòbil i així combinar-les amb altres dades com la ubicació en temps real, útil quan estem buscant tresors.

L'eina que hem fet servir per publicar el mapa es diu Instamaps i permet descarregar les capes una a una, però alguns formats no funcionen. Per això pengem els arxius GPX compatibles amb l'aplicació de mapes lliure [OsmAnd](https://f-droid.org/en/packages/net.osmand.plus/):

**Barris on es recicla...**
- <a href="reciklea-1-dilluns.gpx" download>dilluns</a>
- <a href="reciklea-2-dimarts.gpx" download>dimarts</a>
- <a href="reciklea-3-dimecres.gpx" download>dimecres</a>
- <a href="reciklea-4-dijous.gpx" download>dijous</a>
- <a href="reciklea-5-divendres.gpx" download>divendres</a>

Si teniu problemes, podeu consultar el tutorial de [com importar arxius GPX a OsmAnd](https://www.osmand.net/docs/user/personal/tracks#import-track) (en anglès).

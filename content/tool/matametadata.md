---
title: "Mata Meta Data"
cover: "tool/matametadata/scrubber_logo.png"
description: "Esborra cares i metadades de fotos"

---

**_Mata les metadades!_**

> La NSA _mata basant-se en metadades_, nosaltres _matem les metadades_

Clica la icona de sota per accedir a una aplicació lliure que hem clonat a la nostra web. Té un disseny molt auster, però funciona! I a més, les imatges no surten del teu mòbil o ordinador, es queda tot al navegador ;)

Hem copiat la versió del [Colectivo 406](https://406.neocities.org), que la tradueix al castellà.

[![captura de pantalla de mostra de l'aplicació](demo.png)](./app.html)

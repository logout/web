---
title: "Nosotres"
cover: "img/logout-hl-cadenas-09b.jpg"
---


Nos juntamos a cacharrear y compartir ideas en Can Vies los jueves de 19.00 a 22.30

Nos gusta organizar talleres, como los de navegación segura y de proteccion de la privacidad online.

Nos interesan temas como las licencias abiertas, el anonimato online, la desconexión de las redes sociales privativas y la propiedad de los datos, entre muchos otros.

Software libre y esas cosas, vamos.

Traemos cosas de tecnología a gente politizada y cosas políticas a gente tecnologizada.

---

* 📧 Mail [logout(a)sindominio.net](mailto:logoutARROBAsindominio.net?subject=Hola!)
* 📧 Butlletí [logoutinfo(a)listas.sindominio.net](https://listas.sindominio.net/mailman/listinfo/logoutinfo)
* 🌏 Web [sindominio.net/logout](/)
* 📌 [CSA Can Vies, c/ Jocs Florals 42, Sants, Barcelona](https://www.openstreetmap.org/way/262218049)
* 🕖 [dijous 19:00 - 22:30](/txt/logout.ics)
* 👾 [Logout a Hackerspaces](https://wiki.hackspaces.org/Logout)

---

![façana de can vies](/img/can_vies_febrer_2017.jpg)

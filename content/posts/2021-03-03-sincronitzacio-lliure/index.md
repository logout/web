---
title: "Sincronització autònoma de contactes, calendaris i altres"
date: 2021-03-03
cover: "sincronització/sincronització-autònoma.png"
useRelativeCover: true
coverCaption: "Esquema en A4 apaïsat d'un sistema d'un mòbil i un portàtil que sincronitzen dades amb un servidor de Nextcloud, amb les aplicacions necessàries"
tags: ["logout", "dibuixos", "recursos"]

---

Qui fa servir Google o els productes d'Apple pot estar acostumat a tenir-ho tot sincronitzat entre dispositius sense haver de configurar res, però això té un preu molt alt, que amenaça les nostres llibertats coŀlectives. Per exemple, depenem de servidors i aplicacions dissenyats segons els interessos d'una multinacional top-10 a nivell mundial, que a més conserva còpies sense xifrar de les nostres dades per analitzar-la i poder-nos portar pels seus camins.

Malgrat això, si volem tenir còpies de seguretat de les llibretes de contactes, si fem servir calendaris digitals, gestors de contrasenyes, o volem tenir còpies de les fotos que fem al mòbil, podem fer-ho amb software lliure i independentment de qualsevol compte de Google o Apple. Aquests diagrames mostren les solucions per a un portàtil GNU-Linux o Windows, i per un mòbil Android o derivats.

Apunt: les configuracions de l'aplicació DAVx5 i la del client de Nextcloud del portàtil poden resultar poc intuïtives. Podeu consultar al hacklab més proper o escriure'ns si se us complica :)

![Esquema en vertical d'un sistema d'un mòbil i un portàtil que sincronitzen dades amb un servidor de Nextcloud, amb les aplicacions necessàries](./sincronització/sincronització-autònoma-vertical.png)


Altres versions:
* [Vectorial portable (PDF) - vertical](./sincronització/sincronització-autònoma-vertical.pdf)
* [Vectorial portable (PDF) - apaïsat](./sincronització/sincronització-autònoma.pdf)
* [Vectorial editable (SVG) - vertical](./sincronització/sincronització-autònoma-vertical.svg)
* [Vectorial editable (SVG) - apaïsat](./sincronització/sincronització-autònoma.svg)

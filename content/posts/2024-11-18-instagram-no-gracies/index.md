---
title: "Instagram? No, gràcies."
date: 2024-11-18
cover: "img/instagram-no-gracies.gif"
toc: true
tags: ["fedivers", "instagram", "articles"]
light_mode: true
---

# Instagram? No, gràcies.

_No preocupar-se per la teva privacitat perquè no tens res a amagar és com no preocupar-se per la llibertat d’expressió perquè no tens res a dir._
– Edward Snowden

Som individualitats organitzades en contra de l’expansió de les grans corporacions tecnològiques. Hem redactat aquest escrit perquè veiem com, més i més, col·lectius i projectes autònoms, feministes, antirracistes i anticapitalistes (i també individualitats afins a aquestes idees) **fan servir Instagram com una eina imprescindible** o única de difusió i de comunicació externa o, també, com a manera de relacionar-se socialment. En canvi, els interessos capitalistes i expansionistes d’aquestes plataformes digitals **apunten directament en contra dels objectius que aquests col·lectius** i individualitats volen defensar. No entenem aquest oxímoron.

**El que llegireu no és només aplicable a Instagram**. És també una anàlisi crítica sobre altres xarxes socials com Facebook (Meta), Twitter (ara, X), Whatsapp, Tinder, OkCupid, Bumble, etc. Entenem que **qualsevol servei a Internet que no sigui autogestionat o de programari lliure, funciona sota els interessos foscos de les grans corporacions tecnològiques** que, per darrere d’aquestes xarxes socials, volen limitar el pensament crític i crear un entorn mental on preval l’obediència amb l’objectiu d’expandir el seu monopoli tecnològic. Per tant, davant d’aquesta situació, volem generar un discurs crític i radical en relació amb l’ús d’aquestes plataformes digitals i proposar maneres alternatives de fer servir Internet i de relacionar-nos digitalment.

A continuació exposem algunes de les raons per les quals ens sembla important l’oposició política sobre l’ús i manteniment d’aquestes xarxes socials. A cada apartat, expliquem diferents motius del perquè ens oposem a Instagram. A més, afegim recursos informatius que corroboren les anàlisis incloses en aquest escrit. Després d’aquests apartats, afegim alguns apunts amb informació addicional que ens semblen crucials per entendre l’impacte de l’ús individual i col·lectiu d’aquestes plataformes digitals.

Al final, trobareu un petit text que explica diferents estratègies que proposen un model de relacionar-se digitalment menys intrusiu i més autogestionat, orientat a una perspectiva anticapitalista, antiracista i llibertària d’entendre el món.

Esperem que us sembli interessant i útil.

Gràcies i visca l’autogestió digital!

## **Ens oposem a Instagram perquè...**

### **1. Genera un cànon físic, psicològic i sexual hegemònic**

<div style="display: flex; justify-content: center;" class="post-cover">
    <img src="Canon_fisic.jpg" width=330  alt="Genera un cànon físic, psicològic i sexual hegemònic"/>
</div>

**Els algoritmes prioritzen continguts que s'alineen amb patrons dominants o rendibles.** A través de bucles que retroalimenten discursos hegemònics amb més interacció i visibilitat dels continguts publicats, les xarxes socials suprimeixen altres visions i veus generant, per tant, un cànon físic, psicològic i sexual hegemònic.

A més, **els algoritmes estan pensats per causar addicció i sobre estimulació**, actuant com una droga que deforma la realitat en què i la forma en com ens relacionem amb el món, la gent i nosaltres mateixis. De fet, durant el 2021, [una filtració](https://web.archive.org/web/20231206203901/https://www.elperiodico.com/es/sociedad/20211004/facebook-miente-secretos-han-expuesto-12149743) d’un informe intern de Meta revela que Instagram agreuja els problemes de percepció corporal en un de cada tres adolescents i accentua els nivells d’ansietat i depressió.

Per l’altra banda, aquestes xarxes socials **faciliten l'assetjament sexual** perquè permet l'anonimat, la moderació feble del contingut i l'amplificació algorítmica de diferents tipus de comportament nociu, com el masclisme i l’homofòbia. El mateix Mark Zuckerberg, CEO de Meta, ha hagut de [comparèixer al senat dels Estats Units](https://www.ccma.cat/324/acusen-zuckerberg-de-tenir-les-mans-tacades-de-sang-pels-abusos-a-menors-a-les-xarxes/noticia/3274110/) per donar explicacions sobre els casos d’abús sexual a menors en xarxes com a Instagram.

Amb tot plegat, el 12% dels joves entre 14 i 17 anys va patir violència sexual a través de les xarxes socials en l'any 2023, segons les [conclusions d'un estudi de la Universitat de Barcelona.](https://www.ccma.cat/324/les-xarxes-caldo-de-cultiu-per-a-la-violencia-sexual-el-12-dadolescents-nha-patit/noticia/3283354/)

_Tot i conèixer els efectes perjudicials de les xarxes socials_ [_**no s’ha fet res per mitigar-ho**_](https://www.eldiario.es/tecnologia/arturo-bejar-ex-ingeniero-meta-instagram-mayor-acoso-sexual-historia-humanidad_128_10767446.html)_. Podem, per tant, confiar en una eina creada per homes blancs i rics acostumats a jugar amb la ment de lis consumidoris?_

### **2. Alimenta el capitalisme de vigilància**

<div style="display: flex; justify-content: center;" class="post-cover">
    <img src="Capitalisme_vigilancia.jpg" width="450"  alt="Alimenta el capitalisme de vigilància"/>
</div>

El capitalisme de vigilància és un sistema econòmic on l**es empreses recopilen dades personals de forma massiva amb l'objectiu de predir i influenciar el comportament de lis usuariïs per a benefici propi**. Per exemple, l’empresa [Clearview](https://en.wikipedia.org/wiki/Clearview_AI), fundada per individus vinculats amb **partits i organitzacions de l’extrema dreta**, fa servir el contingut de les xarxes socials per extreure fotografies de les cares de lis usuariïs i alimentar la seva intel·ligència artificial (IA) de reconeixement facial. Amb **l’objectiu d’obtenir 4 fotos de cada habitant del planeta**, els seus clients estan dispersos per tot el món, incloses empreses privades i règims autocràtics, com s’explica al documental «Tu cara es nuestra», produït per Televisió Espanyola. Al mateix documental, es pot veure un tast de com sorprenent és aquest programari i s’hi explica el tema en profunditat.

Un altre cas és el de [Cambridge Analytica](https://en.wikipedia.org/wiki/Cambridge_Analytica), empresa dedicada a extreure informació de les xarxes socials. **La seva actuació va ser clau en molts moments** com, per exemple, durant la campanya del [Brexit al Regne Unit i les eleccions per president de Donald Trump als Estats Units](https://en.wikipedia.org/wiki/Facebook%E2%80%93Cambridge_Analytica_data_scandal). Inclús van oferir els seus serveis a partits com [Vox](https://elpais.com/elpais/2018/05/09/actualidad/1525879892_390255.html). Gent clau d’aquesta empresa, com [Steve Bannon](https://en.wikipedia.org/wiki/Steve_Bannon), estan també relacionats amb l’extrema dreta i han mantingut reunions amb [Marine Le Pen, Matteo Salvini](https://prospect.org/power/le-pen-salvini-europe-s-far-right-increase-numbers-european-parliament-bannon-stays-close-hand/) i altres.

Però les estratègies de desinformació generades a través de les dades que generem a les xarxes socials no acaba aquí —[i agafa mil formes](https://www.scielo.cl/scielo.php?script=sci_arttext&pid=S0718-48672019000200011)—. Al Brasil, per exemple, una campanya de desinformació orquestrada des dels Estats Units [a través del Whatsapp](https://www.elconfidencial.com/mundo/2018-10-25/jair-bolsonaro-fake-news-elecciones-brasil_1635334/) va ajudar a Jair Bolsonaro a arribar al poder en 2018.

I no només això. **Les dades generades de i per usuari****ï****s pot acabar en mans de qualsevol**, per exemple a la web [https://antifawatch.net/](https://antifawatch.net/). Aquesta pàgina està controlada per grups neonazis que recopilen informació personal, fotografies i altres dades sobre persones i col·lectius antifeixistes.

_Tot i que pensem que no estem penjant res important,_ _**en el món de les dades tot compta**__. Per tant, hem de ser conscients que les nostres dades són valuoses i podem estar col·laborant inconscientment amb projectes i idees en què estem en contra._

### **3\. Amplifica la polarització i la desinformació**

<div style="display: flex; justify-content: center;" class="post-cover">
    <img src="Nazi.jpg" width="450"  alt="Amplifica la polarització i la desinformació"/>
</div>

Conegut com a [_radicalització algorítmica_](https://en.wikipedia.org/wiki/Algorithmic_radicalization)_,_ aquest **mecanisme genera recomanacions a continguts extrems i morbosos amb l’objectiu de maximitzar el temps que lis usuariïs dediquen a les xarxes socials**. Com a resultat, lis usuariïs són manipuladis contínuament, arribant a opinions més polaritzades sense cap mena d’anàlisi sobre els interessos que existeixen per darrere d’aquests discursos_._

La radicalització algorítmica succeeix a la majoria de xarxes socials i està **directament relacionada amb l’augment de l’homofòbia, islamofòbia o de l’expansió de l’extrema dreta a tot el món**. En efecte, en la mateixa filtració que va revelar un [informe intern de Meta](https://web.archive.org/web/20231206203901/https://www.elperiodico.com/es/sociedad/20211004/facebook-miente-secretos-han-expuesto-12149743), dona a entendre com l’algoritme de recomanació de contingut promociona el contingut polaritzat. Fàcilment, usuariïs comencen veient vídeos de receptes de cuina i acaben amb continguts de l’extrema dreta.

Endemés, aquestes xarxes socials també serveixen com a plataforma que **afavoreix la proliferació de discursos d’odi i populisme**, on la “nova extrema dreta” ha trobat una nova veta a explotar per captar la joventut. Per exemple, **Vox va gastar 300 euros en una publicitat al metro de Madrid** dient que [un menor d’edat estranger cobra 4700 euros al mes](https://www.eldiario.es/madrid/vox-coloca-publicidad-electoral-estacion-sol_1_7840154.html). Aquesta campanya es va fer viral, fent-los guanyar milers d’euros en publicitat a les xarxes socials, a més de visibilitat i votants.

Altres campanyes es basen en [analitzar dades que són públiques a les xarxes socials per establir una estratègia de desinformació](https://www.bbc.com/mundo/noticias-43472797), com feia l’empresa Cambridge Analytica, esmentada a l’anterior punt. Per exemple, si detecten que un sector de la població indecís és homòfob o racista, engeguen una campanya de Fake News dient que les migrants reben més ajudes que gent “nascuda aquí" o que “l’educació sexual a les escoles [ensenya als nens a ser homosexuals](https://verne.elpais.com/verne/2018/10/18/mexico/1539847547_146583.html)”.

En tot aquest ecosistema, **els discursos d’homes blancs cis-hetero es posiciona fàcilment per sobre d’altres**. A poc a poc, l’extrema dreta ha anat envaint les xarxes socials, ja que el seu discurs afavoreix el capitalisme neoliberal.

La moderació doncs no és imparcial: crea fenòmens com els [Fachatubers](https://www.lamarea.com/2024/02/20/los-fachatubers-al-descubierto-un-documental-muestra-las-estrategias-de-la-red-ultra/), fomenta discursos d’odi com l’lgtbi-fòbia i aposenta l’auge de l’extrema dreta.

_Podem confiar en un sistema moderat per gent als qui aquests discursos afavoreixen?_ _**Instagram dona visibilitat als discursos neonazis**._ _No siguis còmplice de les seves estratègies manipuladores_

### **4\. Filtra informació a la policia**

<div style="display: flex; justify-content: center;" class="post-cover">
    <img src="Filtra_informacio.jpg" width="450"  alt="Filtra informació a la policia"/>
</div>

**Els agents de la policia poden fer servir les xarxes socials per monitorar individus** mitjançant l'anàlisi de publicacions i el seguiment de dades de geolocalització. A més, l'ús d'eines algorítmiques poden identificar i perfilar persones considerades sospitoses o moviments en grup, públic o privats, de xarxes socials que tenen més activitat. Entre les dades que investiguen, els agents de la policia i de vigilància de l’estat estan atents a:

1.  **Qui ha estat a on i amb qui**, inclús gent que no forma part de cap mena de xarxes socials. Això passa quan fotografies amb persones que no tenen xarxes socials són pujades sense el seu coneixement.
2.  **Els hàbits de lis usuariïs**, incloent-hi a quines hores es connecten, des d’on, quina mena de continguts els agrada, etc.

Per prendre algunes d’aquestes accions de vigilància, **la policia necessita una autorització judicial que justifiqui la manipulació de dades privades**. Normalment, aquestes ordres judicials són demanades amb la intenció de revelar la informació de qui està darrere d’un compte en concret, i fer servir el contingut pujat com a prova d’un judici.

Tot i això, amb el sistema d’espionatge digital [Pegasus](https://en.wikipedia.org/wiki/Pegasus_(spyware)), desenvolupat per l’empresa israeliana NSO Group, es pot observar com molts cossos policials, inclús l’espanyol, **fan servir eïnes de vigilància** contra la població **sense cap ordre judicial**, [tal i com s’ha vist en reiterades ocasions](https://directa.cat/the-wire/).

Entre els casos més coneguts, destaquen les [suplantacions d’identitat fetes pels mossos d’esquadra](https://directa.cat/app/uploads/2020/10/Directa-510.pdf) (destapat per La Directa) a les xarxes socials amb l’objectiu d’espiar diferents moviments socials a Catalunya, recopilant informació i inclús realitzant interaccions amb les persones d’interès.

A més, empreses com **Meta o Google tenen les seves pròpies vies perquè la policia pugui consultar aquestes dades** en una ostentació de col·laboració policial més que plaent. [Aquí](https://transparency.fb.com/reports/government-data-requests/) es poden veure les peticions on Meta ha donat oficialment informació d’usuàriïs a diversos estats. Google et permet veure una part del que saben de tu a la [pàgina d’activitat del compte](https://myactivity.google.com/). Buscant dins d’aquesta pàgina, podem trobar apartats com l’historial de cerques, activitat en diverses apps o geolocalització. Aquesta informació pot ser demanada sota ordre judicial. Animem a lis lectoris a fer un cop d’ull i configurar aquesta part de la compta de Google, tenint en compte que realment no sabem quina informació pot seguir guardant, tot i que es pot deshabilitar el seguiment d’historials.

A més, encara existeixen altres eines de pagament, com [FogReveal](https://en.wikipedia.org/wiki/Fog_Reveal), que presten serveis a la policia per analitzar les xarxes socials, donant informació extra sobre lis usuariïs sense la necessitat de fer servir programes d’espionatge digital com el Pegasus. Aquestes eines estan a la venta i proporcionen aquesta informació no només a cossos policials, sinó a qui ho pugui pagar.

_El capitalisme de vigilància és el somni de qualsevol cos policial, i la teva mera presencia allá ho alimenta! No hi col·laboris!_

### **5\. Fa mal al planeta**

<div style="display: flex; justify-content: center;" class="post-cover">
    <img src="planeta.jpg" width=330  alt="Fa mal al planeta"/>
</div>

El que coneixem com a núvol és, en realitat, una quantitat enorme d’ordinadors interconnectats, entre ells en llocs anomenats _datacenters_. En els _datacenters,_ aplicacions com Instagram emmagatzemen totes les nostres dades, com fotografies i converses. Aquests _datacenters_ són propietat de grans empreses com Amazon, Microsoft o Google, i estan repartits per tot el planeta amb l’objectiu d’oferir un servei ràpid i escalable des de la persona més rica fins a la més pobra. [El 70% del tràfic d’Internet](https://www.newtral.es/energia-centros-datos-contaminacion-renovables/20201111/) passa pels datacenters d’aquestes tres empreses.

**Els** _**datacenters**_ **tenen un gran consum d’energia**, equivalent a [1% del consum energètic mundial](https://www.datacenterknowledge.com/energy/study-data-centers-responsible-1-percent-all-electricity-consumed-worldwide). Per l’altra banda, tot [l’ecosistema depenent de les tecnologies de la comunicació](https://corporate.enelx.com/es/stories/2021/12/data-center-industry-sustainability) és responsable gairebé pel 2% de tota l’emissió de diòxid de carboni anual en una escala global —tot i que caldria revisar dades més actuals; amb el tema IA ha canviat força!—. A més, necessiten grans quantitats d’aigua per poder refrigerar tots els ordinadors. Per exemple, **s’estima que el** _**datacenter**_ **de Meta a Talavera de la Reina, una zona en perill de sequera, consumirà** [**600 milions de litres d’aigua**](https://tunubesecamirio.com/recursos/) **potable anuals.**

_Seguim creixent desmesuradament i gastant recursos naturals com si fossin infinits. Realment necessitem aquesta quantitat enorme de dades i d’informació?_

### **6\. Dona suport a Israel i censura Palestina**

<div style="display: flex; justify-content: center;" class="post-cover">
    <img src="Censura_Palestina.jpg" width="450"  alt="Dona suport a Israel i censura Palestina"/>
</div>

Ja no és només Instagram, sinó **la majoria de grans corporacions estatunidenques que donen suport al genocidi del poble Palestí i l’apartheid d’Israel**. Concretament, la xarxa social de Meta, [col·labora directament amb el govern israelià](https://theintercept.com/2016/09/12/facebook-is-collaborating-with-the-israeli-government-to-determine-what-should-be-censored/) per decidir el que ha de ser censurat. [Segons Human Right Watch,](https://elpais.com/planeta-futuro/2023-12-21/human-rights-watch-acusa-a-meta-de-censurar-contenidos-de-apoyo-a-palestina-en-instagram-y-facebook.html#) eliminen sistemàticament el contingut pro-Palestina o que critiqui a Israel.

Però no es limita a això. El [projecte Nimbus](https://en.wikipedia.org/wiki/Project_Nimbus)_,_ en el qual participen Google i [Amazon](https://www.972mag.com/cloud-israeli-army-gaza-amazon-google-microsoft/) juntament amb el govern d’Israel, **proporcionen una infraestructura tecnològica potent per seguir** [**perpetuant el genocidi**](https://www.notechforapartheid.com/)**.**

Aparentment, Google està ajudant a la [localització i identificació](https://www.elsaltodiario.com/genocidio/ejercito-israeli-utiliza-infraestructura-google) d’objectius i proporcionant eines de reconeixement facial. [Algunes de les tecnologies](https://www.ccma.cat/324/aixi-fa-servir-la-intelligencia-artificial-israel-per-escollir-objectius-per-matar-a-gaza/noticia/3287737/) d’IA (executada per mitjà dels servidors de Google) que fa servir l’exèrcit Israelià inclouen:

-   [Lavender](https://www.972mag.com/lavender-ai-israeli-army-gaza/), un sistema amb uns paràmetres poc clars que ajuda a decidir si alguna persona té algun tipus de relació amb o forma part del Hamàs. Per exemple, depenent si una persona està identificada com a home o dona, o si canvia molt de telèfon, pot provocar que bombardegin casa seva.
-   [Where is Daddy](https://www.businessinsider.com/israel-ai-system-wheres-daddy-strikes-hamas-family-homes-2024), un sistema de geolocalització amb IA capaç de seguir els moviments de persones fins a casa seva per després bombardejar-la.
-   [The Gospel](https://www.theguardian.com/world/2023/dec/01/the-gospel-how-israel-uses-ai-to-select-bombing-targets), un sistema d’IA que s’encarrega de detectar quins edificis estan controlats per Hamàs per poder-los bombardejar.

Tot i que aquests sistemes no siguin només de Meta, les grans corporacions tecnològiques juguen un paper fonamental, directa o indirectament, en el genocidi. **Meta, per exemple, com ja hem vist, comparteix informació inclús de qui hi ha en grups de Whatsapp per a poder** **[seleccionar els objectius de Lavender](https://blog.paulbiggar.com/meta-and-lavender/#:~:text=changing%20addresses%20frequently.-,%5B2%5D,-Though%2C%20I%20believe)**.

_Què hauria passat si algun règim genocida de la Segona Guerra Mundial hagués tingut tota la informació que existeix ara a les xarxes?_

### **7\. Funciona com una droga**

<div style="display: flex; justify-content: center;" class="post-cover">
    <img src="Droga.jpg" width="330"  alt="Funciona com una droga"/>
</div>

**Instagram i altres xarxes socials provoquen addicció**. De fet, [Meta ha estat denunciada](https://www.genbeta.com/a-fondo/meta-ha-sido-denunciada-sus-redes-sociales-adictivas-ciencia-corrobora-que-efectivamente-instagram-otras-causan-adiccion) per les implicacions addictives que provoquen les seves xarxes socials. Per altre banda, un [informe filtrat en una demanda de TikTok](https://www.3cat.cat/324/tiktok-sap-que-et-pot-fer-addicte-en-poc-mes-de-mitja-hora-i-no-en-protegeix-els-menors/noticia/3316119/) demostra que la companyia sap que **en poc menys de 30 minuts pot fer addicte a una persona.**

I com fan això? De la mateixa manera que ho fan els jocs d’atzar, o les drogues: les xarxes socials alliberen petits estímuls intermitents per mitjà dels “likes” i altres interaccions que ens provoquen la secreció de petites dosis de dopamina. D’aquesta manera, amb aquesta contínua injecció d’hormones del plaer, aquests estímuls es converteixen en addicció.

L’addicció neurològica no és poca cosa. Als anys 50, un estudi va aconseguir que un conjunt de rates amb elèctrodes al cervell acabessin preferint aquests estímuls molt per sobre de menjar o dormir, i que acabessin pitjant compulsivament la palanca fins a morir extenuades ([la caixa de Skinner](https://www.psicologia-online.com/la-caja-de-skinner-en-que-consiste-este-experimento-5461.html)).

Aquí, el problema recau en el fet que **manipulen les nostres ments a nivells molt primitius**: saben com es comporta el nostre cervell i posen en pràctica allò que van aprendre primer en rates de laboratori i després en consumidors. En el llibre de Marta Peirano, _El enemigo conoce el sistema_, es descriu aquest procés i explica com a les nòmines de les GAFAM (Google, Amazon, Facebook, Apple, Microsoft) hi ha els millors sociòlegs, psicòlegs —i podem sospitar que altres branques de la ciència— sortits de les universitats més elitistes de EUA.

_Han jugat amb les nostres ments i continuen jugant com si fóssim rates de laboratori. Hem de deixar que això passi?_

### **8\. Fa diners amb la teva vida**

<div style="display: flex; justify-content: center;" class="post-cover">
    <img src="Fa_diners.jpg" width="450"  alt="Fa diners amb la teva vida"/>
</div>

Les grans corporacions tecnològiques busquen obtenir el màxim benefici i créixer exponencialment. Tot i això, els seus productes segueixen sent aparentment gratuïts. Però això no és res més que una paradoxa: **el producte no és la xarxa social en si, el producte ets tu**. Aquestes corporacions venen les dades de lis usuariïs per lucrar-se. A més, quan un contingut és penjat a Instagram, aquest [deixa de ser propietat de la persona que el penja](https://rrs-law.medium.com/a-qui%C3%A9n-pertenece-lo-que-publico-en-instagram-426f46a02614), i passa a ser de Meta que [ho pot fer servir pel que vulgui](https://www.3cat.cat/324/com-evitar-que-les-teves-publicacions-a-instagram-i-facebook-sutilitzin-per-entrenar-ia/noticia/3295250/).

Amb les dades de lis usuariïs, **les xarxes socials venen aquesta informació a empreses** que es dediquen al _data mining,_ o [extractivisme de dades](https://www.lavanguardia.com/internacional/vanguardia-dossier/revista/20240229/9509092/extractivismo-datos-nuevo-colonialismo.html). Amb aquestes dades es poden fer estudis de població, crear anuncis personalitzats molt més eficaços perquè mossegui l’esquer, influir en eleccions. [Cada usuari de Meta és monitorat](https://www.consumerreports.org/electronics/privacy/each-facebook-user-is-monitored-by-thousands-of-companies-a5824207467/) per centenars de companyies dels més variats orígens i propòsits.

De fet, a principis del 2023, [una filtració](https://gizmodo.com/cmg-local-solutions-ads-listening-on-devices-1851102426) i [diverses investigacions](https://gizmodo.com/these-academics-spent-the-last-year-testing-whether-you-1826961188) revelaven que **algunes apps dels nostres telèfons** (entre elles, Instagram) **ens escolten permanentment**, analitzant el que diem per, a posteriori, creuar la informació amb el nostre perfil a Internet i revendre’l a empreses dedicades a la publicitat. Trobem a les GAFAM entre les que [fan servir aquest mètode](https://gizmodo.com/pitch-dek-gives-new-details-on-companys-plan-to-listen-to-your-devices-for-ad-targeting-2000491095).

_En un món capitalista no hi ha res gratuït. Les nostres vides no tenen preu. No les venguis a especuladors digitals!_

## **Fins i tot,** **la lluita contra Instagram és una lluita anticapitalista**

Lluitar contra Instagram és una part més de la lluita transversal de l’anticapitalisme. Instagram ens fa addictes, ens manipula, ven la nostra intimitat, col·labora amb el genocidi a Palestina, [explota els seus treballadors](https://www.elsaltodiario.com/barcelona/condenada-subcontrata-facebook-discriminar-salarialmente-segun-nacionalidad-trabajadores) i genera una gran quantitat de CO2 i de consum d’aigua. A més, perpetua els estereotips de gènere i el patriarcat, és usat per bandes neonazis per assolir el poder i identificar migrants sense papers.

No ens hem d’oblidar que Meta i altres corporacions són grans empreses estatunidenques que cotitzen a borsa i que tenen l’objectiu d’aconseguir els màxims beneficis. Per tant, les xarxes socials són una part important de l’engranatge que fa girar la maquinària capitalista.

Instagram no es pot fer servir com una eina política perquè és una arma que apunta directament a nosaltres. I fer-ho servir és convertir-nos en còmplices de tot això. Fem-ho fàcil:

**Digues NO a Instagram i NO al capitalisme de vigilància.**

<div style="display: flex; justify-content: center;" class="post-cover">
    <img src="bota.jpg" width="750"  alt="Digues NO a Instagram i NO al capitalisme de vigilància."/>
</div>

## **I ara què? Alternatives a Instagram**

La principal dificultat que ens trobem a les xarxes socials com Instagram és que són totalment opaques. Això passa perquè:

-   **La informació està centralitzada** a servidors, en mans de grans multinacionals, on es guarda la nostra informació sense saber qui la té. A més, només es pot compartir contingut amb la gent que forma part de la mateixa xarxa social.
-   **No sabem com funciona per dintre**. L’algoritme és un secret misteriós. No sabem com ho fan per recomanar-nos contingut, si el micròfon ens espia o si estan fent servir el que pengem per entrenar una IA assassina.
-   **Les dades ja no són teves** i passen a les mans d’empreses de tota classe, perquè, com hem vist, **tu ets el producte.**
    **Per sort, existeixen xarxes socials que poden ser una alternativa a aquest model opac i centralitzat**. Són les xarxes socials del _**Fedivers**_. Segons Wikipedia, el Fedivers és un mot _que s'utilitza habitualment i de forma informal per a referir-se a una federació oberta de servidors de xarxa social_.

Deixant-nos de paraules estranyes, el _Fedivers_ és bàsicament un grup de xarxes socials que es poden interconnectar entre elles (com si des d’Instagram poguessis escriure a TikTok, i a més poguessin existir servidors diferents d’Instagram a molts llocs). A les xarxes socials del [Fedivers](https://video.anartist.org/w/9dRFC6Ya11NCVeYKn8ZhiD):

-   **La informació no viu en un únic servidor** i es reparteix entre els servidors que estan federats entre ells. Aquests servidors estan repartits arreu del món i qualsevol persona, amb els coneixements tècnics suficients, en podria tenir un. Per exemple, podria haver-hi un servidor al teu barri per donar servei a la gent sòcia de la biblioteca. Xarxes socials de proximitat!
-   **És programari lliure**, el que significa que alguna persona amb els coneixements tècnics suficients pot mirar com està fet el programa, buscar i solucionar errors, millorar-lo i fer aportacions en una forma molt més participativa i distribuïda de crear les aplicacions que fem servir.
-   **Les dades et pertanyen**, ja que les polítiques de privacitat solen ser menys abusives. A més, sempre pots muntar-te el teu propi servei, o buscar-ne una persona amb qui confiïs. Serveis de proximitat i de qualitat 👌.

Algunes de les alternatives més conegudes són:

-   Mastodon: substitueix Twitter (ara X)
-   Pixelfed: substitueix Instagram
-   Lemmy: substitueix Reddit i fòrums públics
-   Peertube: substitueix Youtube
-   Moltes més! Us aninem a fer la vostra investigació i descobriments.

Hi ha moltes vies per entrar al fedivers:
- El portal [fedi.cat](https://fedi.cat), amb nodes de parla catalana, fòrum, i informació de benvinguda.
- La pàgina [https://jointhefediverse.net/](https://jointhefediverse.net/) on podeu trobar més informació.
- Properament: [La Furgo](https://sindominio.net/lafurgo), projecte per facilitar el trasllat de «xarxes socials» capitalistes cap a les comunitàries.
- El [vídeo «Què és el Fedivers»](https://framatube.org/w/9dRFC6Ya11NCVeYKn8ZhiD), en anglès amb subítols a moltes llengües. Allotjat a Peertube!

_Hem d’apostar per servidors petits, federats, autogestionats i sostenibles davant l’emmagatzemament massiu, centralitzat i contaminant._

## **I què passa si no puc desaparèixer sense més?**

**Pot passar que**, pel motiu que sigui, **necessiteu tenir presència a les xarxes socials com Instagram**, ja sigui per intentar arribar a més gent o perquè és l’única forma de posar-vos en contacte amb altres col·lectius.

Existeixen propostes com la [RAP](https://rap.komunikilo.org/ca/mesures.html) que ens poden donar algunes idees si això passa. Proposen que, si hem de seguir existint en el que anomenen _xarxes socials publicitaries_, almenys es prenguin les següents mesures. Resumidament:

-   **Usar sense promocionar**: Evitar al màxim el rastreig i fer servir quantes menys funcions possibles. Per exemple, no incrustar-les a les nostres webs: el típic botó de m’agrada que es posa a la web i serveix per rastrejar usuariïs.
-   **Evitar contingut exclusiu**: Publicar contingut exclusiu a mitjans publicitaris és una manera indirecta d'incitar a altres persones a participar-hi. Per exemple, en comptes de penjar un cartell o fer un post, pots publicar un enllaç amb la informació penjada en algun altre medi (com ara un web) que estigui sota el nostre control preferiblement.
-   **Evitar contribuir econòmicament**: Pagar per posar abans les nostres publicacions és finançar i participar d'un sistema publicitari que explota l'atenció de les persones i genera un entorn digital tòxic.

Tot i que aquests punts estan formulats des d’un punt de vista de lluita contra el sistema publicitari, són unes bones bases per fer servir les xarxes socials si no tens més remei. Altres mesures que podrien afegir-se per reduir el seu abast inclouen:

-   Reduir les interaccions al mínim, per evitar fer grafs socials, estudis, etc.
-   No penjar mai fotos on surtin cares i, en general, cap mena de fotografia.
-   No fer-les servir com a eina principal de comunicació amb altres col·lectius o persones
-   No fer-les servir com a eina principal de difusió
    **La millor forma de no intervenir en tot el mecanisme és no tenir-hi cap compte**, i en cas de tenir-ne, reduir el seu ús al mínim. Inclús, penjar-hi coses que siguin sempre enllaços externs (a mitjans que controlis). I si algú t’hi parla, redirigir al teu canal de comunicació principal.

## **El futur de la nostra privacitat**

Recentment, s’ha impedit la publicació d’una llei Europea ([chat control](https://www.patrick-breyer.de/en/council-to-greenlight-chat-control-take-action-now/)) que volia prohibir el xifratge d’extrem a extrem amb l’excusa de protegir la infantesa de la pederàstia. _Grosso modo_ impedia la comunicació segura entre persones sense possibilitat de poder accedir al contingut del missatge. Aquesta llei [encara s’està debatent](https://www.patrick-breyer.de/en/posts/chat-control/) i pot acabar sent aprovada, cosa que suposaria la impossibilitat de tenir comunicacions segures entre persones, violant l’article 7 de la [carta dels drets fonamentals Europeus](https://www.europarl.europa.eu/charter/pdf/text_es.pdf) (el dret al respecte de la vida privada i familiar).

Per altra banda, més d’una vegada s’ha volgut fer servir el fet que una persona [faci servir comunicacions segures](https://help.riseup.net/ca/about-us/press/security-not-a-crime) com a indici del fet que era una persona delinqüent. És a dir, s’està criminalitzant l’ús d’eines de confiança que permeten establir comunicacions segures i privades, titllant lis usuariïs de delinqüents.

S’han de protegir les comunicacions segures, la privacitat i seguretat de lis usuariïs, tant com protegim la llibertat d’expressió. De fet, com més gent faci servir aquestes eines al seu dia a dia, menys sospitós serà el moment quan algú que ho necessiti fer servir per necessitat. Fer servir eines com Whatsapp, en comptes d’altres que sí que es preocupen de la privacitat de lis usuariïs (com Signal, Tox, Matrix/Element, etc.), fa que aquells que les fan servir siguin sospitosos d’estar amagant alguna cosa. Ja no és que ens preocupem de nosaltres, sinó que ens preocupem de totis.

Davant d’un futur que tendeix cap a una distopia on ens governi la dictadura del capitalisme de vigilància, hem de fomentar eines que ens alliberin i sabotejar aquelles que ens oprimeixin. Apostem per l’autogestió digital, les cures de seguretat i privacitat, i el programari lliure!

## **Disclaimer (seguim sent nosaltres)**

<div style="display: flex; justify-content: center;" class="post-cover">
    <img src="nosaltres.jpg" width="550"  alt="seguim sent nosaltres"/>
</div>

Després de llegir aquesta distopia és normal sentir-se malament per fer servir Instagram. **No és la nostra intenció fer un judici de valors a la gent per fer servir aquestes xarxes**. Entenem, que dins d’aquest món en el qual vivim, existeixen moltes contradiccions en què tothom cau i no per això som millors o pitjors.

**La intenció del text és purament divulgativa**: vol generar discurs i coneixement en un moment històric que les tecnologies són cada cop més importants, però hi ha un gran buit de consciència de com funcionen. Per això és important lluitar contra aquest desconeixement i fomentar l’empoderament digital de les persones.

Elaborem les nostres idees analitzant la informació que tenim a l’abast, **des d’una perspectiva feminista, antiracista, anticolonial i llibertària**. Si ets una persona interessada en aquesta matèria, amb ganes d’aprendre o generar discurs, et pots posar en contacte amb nosaltres per anar formant una xarxa solidària i continuar avançant en aquests temes.

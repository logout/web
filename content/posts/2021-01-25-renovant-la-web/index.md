---
title: "Estem millorant la web"
date: 2021-01-25
cover: "under-construction-pikachu.gif"
useRelativeCover: true
tags: ["web","logout","hugo","presentacions"]
---

Estem intentant publicar les presentacions que teníem escampades en un subapartat d'aquesta web, [../diapos](https://sindominio.net/logout/diapos/).
Estem fent servir [repositori de git de sindominio](https://git.sindominio.net/logout/) (o _forja_)
per construir i publicar tant la web com el subapartat de presentacions o diapositives.

Per ara tenim [un repositori per la web principal](https://git.sindominio.net/logout/web) i un altre [per l'apartat de presentacions](https://git.sindominio.net/logout/diapos).

Estem intentant que funcioni, però mentrestant, us deixem un gif clàssic!
Ah, i una web temporal que sí que funciona: [presentem.prou.be](https://presentem.prou.be)

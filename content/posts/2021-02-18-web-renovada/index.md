---
title: "Vuelve la web y se suman nuestros papiros"
date: 2021-02-18
cover: "code.jpg"
useRelativeCover: true
coverCaption: "Photo by [Markus Spiske from Pexels](https://www.pexels.com/photo/black-laptop-computer-turned-on-showing-computer-codes-177598/)"
tags: ["web","logout"]
---

Vuelve nuestra web 100% estatica, generada con [hugo][1].

Se suma a la web la seccion de [papiros del logout][2], donde puedes
encontrar una seleccion de presentaciones sobre nuestros temas de interes.

Si te da curiosidad entender como funcionan nuestras web, podes encontrar
el contenido [publicado aqui][3], y el de los [papiros aqui][4].

Seguimos siempre en Can Vias los Jueves a partir de las 19hs

Te esperamos !

[1]: https://gohugo.io/
[2]: https://sindominio.net/logout/diapos/#/
[3]: https://0xacab.org/logout/web
[4]: https://0xacab.org/logout/diapos

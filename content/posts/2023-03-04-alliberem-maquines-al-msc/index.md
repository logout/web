---
title: "Alliberem mòbils i portàtils al Mobile Social Congress"
date: 2023-03-04
cover: "esquema_distros_android.jpg"
useRelativeCover: true
tags: ["activitats","msc", "mwc"]

---

El dissabte 4 de març hem participat al Mobile Social Congress fent una mica el mateix que fem al hacklab quan veniu a veurer'ns! Hem portat pendrives per instaŀlar unes quantres variants de GNU/Linux i recursos per ensenyar aplicacions alliberadores a Android.

Hem revisat un NUC reciclat d'una immobiliària que l'havia llençat, hem explicat conceptes bàsics de software lliure, obert camins a comunitats i al món de les xarxes federades i d'aplicacions lliures, i instaŀlat algunes aplicacions. La nostra recepta bàsica és [F-Droid](https://f-droid.org) + [NewPipe](https://f-droid.org/packages/org.schabi.newpipe/). Un repositori d'aplicacions lliures i una app per escoltar música i vídeos des de Youtube (i Bandcamp i Peertube!) sense anuncis i amb llistes de reproducció locals, la possibilitat de descarregar cançons de forma fàcil, etc.

Al pati de sota estaven els companys de [Restarters BCN](https://restartersbcn.info) arreglant petits electrodomèstics i aparell electrònics.

Vam anunciar-ho a [bcn.convoca.la]( https://bcn.convoca.la/event/alliberem-mobils-i-portatils-de-les-urpes-del-capital) i al [fedivers](https://bcn.convoca.la/event/alliberem-mobils-i-portatils-de-les-urpes-del-capital)
Ens veiem a la propera!

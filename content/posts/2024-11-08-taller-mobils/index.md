---
title: "Taller pràctic de mòbils a la Revoltosa"
date: 2024-11-08
cover: "mobils-revoltosa.jpg"
useRelativeCover: true
toc: false
tags: ["activitats", "formacions", "seguretat", "mòbils"]

---

    📅 dimecres 13 de novembre de 2024
    🕖 19h - 21h
    📍 CSOA La Revoltosa

Exploraremos como funciona un telefono y la red telefonica. Hablaremos de las buenas practicas para un maximo de privacidad y configuraremos las opciones para un telefono y unas comunicaciones mas seguras!

No hace falta tener conocimiento previo!

Llegar puntual -.- !!

🚉 \<M\> Clot L1 L2



<script src='https://bcn.convoca.la/gancio-events.es.js'></script>
<gancio-event baseurl='https://bcn.convoca.la' id=7029></gancio-event>
<noscript>https://bcn.convoca.la/event/taller-pratico-de-ciberseguridad-en-smartphone</noscript>


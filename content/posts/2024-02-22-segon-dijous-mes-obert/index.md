---
title: "Ens reservem un dijous al mes sencer per vosaltres!"
date: 2024-02-22
cover: "cartell.png"
useRelativeCover: true
tags: ["activitats", "horaris"]

---

Des de l'inici al Logout hem buscat aportar al nostre entorn militant suport tècnic
en seguretat informàtica, en alliberar i recuperar màquines, i en resistir el
capitalisme de la vigilància, que acumula més poder que molts estats, i coŀlabora
de gust amb la repressió estatal.

A part del suport, nosaltres també tenim els nostres projectes, i a més, ens agrada
trobar-nos simplement i comentar la jugada. Això feia que unes activitats xoquessin
amb les altres... no es pot estar concentradi programant o configurant un servidor
alhora que acollim una persona, o preguntem en veu alta si algú sap com abordar el seu
problema, etc.

Llavors! Hem decidit dedicar el 2n dijous de cada mes, en horari habitual de hacklab,
de 19:30 a 22:30, a acollir visites i persones interessades, i a donar suport a
persones o coŀlectius [que ens hagin avisat abans per mail](mailto:logout@sindominio.net?subject=hola%20logout!).
Així podem dedicar-vos el temps de qualitat que volem compartir :3

Podeu venir per molts motius, però us diem alguns dels que han apropat algunes persones:
- millorar la comuninació digital interna d'un coŀlectiu, les mesures de seguretat
- arreglar un problema d'arrencada d'un portàtil amb algun gnu-linux, un problema d'actualització, xifrar-lo, treure-li window$, ...
- conèixer què fem, compartir inquietuds i venir a dedicar temps a algun projecte personal que vulgueu compartir
- instaŀlar un android net sense coses inútils i espia de google, samsung, xiaomi... (només en mòbils compatibles)
- aprendre d'algun tema que algú del logout conegui, i treballar-hi juntis o apuntar-se a algun projecte en curs
- migrar els correus d'un coŀlectiu i els xats a servidors radicals de confiança
- proposar-nos una activitat per organitzar juntis
- ... tu diràs! :D

El proper serà el 14 de març, l'altre l'11 d'abril... Vols venir?

Fins aviat!


---
title: "DesertSeq Hackmeeting 2023"
date: 2023-09-29
cover: "hackmeeting2023.jpg"
useRelativeCover: true
tags: ["activitats","hackmeeting"]

---

El setembre passat es va celebrar el [Hackmeeting 2023](https://es.hackmeeting.org) ibèric. Va tenir lloc al CSO La Squatxeria a L'Hospitalet de Llobregat durant els dies 29,30 de setembre i 1 d'octubre.

La Squatxeria es va omplir de tallers i xerrades durant aquest cap de setmana. Persones vingudes de tota la peninsula i d'Europa van omplir l'espai amb debats frikis, polítics i programari lliure. La festa del dissabte a la nit va tenir lloc al CSO Nabat 3 a tan sols uns carrers de la Squatxeria.

Com Hacklab ens vam implicar en l'adequació de l'espai per a fer les xerrades, sales per a dormir, preparar les samarretes amb el logo d'enguany, els menjars i les begudes de la barra. Va ser una feina de molts mesos que va donar els seus fruits en un sol cap de setmana.

El Hackmeeting és un espai autogestionat de trobada on compartir inquietuds tecno-polítiques. Des de la primera trobada al 1998 fins avui, ha estat un espai perquè gent amb o sense coneixements tècnics pugui trobar-se, plantejar dubtes, generar debats o compartir coneixements en un entorn on lo social i la tecnologia són les protagonistes.


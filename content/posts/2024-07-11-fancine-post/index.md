---
title: "Fanzine disponible online!!"
date: 2024-07-11
cover: "fancines.jpg"
useRelativeCover: true
tags: ["fanzine"]

---

Per fi tenim penjat el nostre estimat fanzine "Mucho móvil, poca diversión" a internet. El podeu trobar en aquest enllaç:

<https://sindominio.net/logout/fanzine/>

Properament, traurem la tercera versió amb correccions i una versió en català!!

`←] logout`

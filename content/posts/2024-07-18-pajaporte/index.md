---
title: "El tema pajaporte"
date: 2024-07-18
cover: "img/memes/think-of-the-children.jpg"
toc: true
tags: ["fedivers", "articles"]

---

Después del [éxtasis][lo1] [inicial][lo2] donde analizamos el tema [Pajaporte][pajaporte]
*en caliente*, nos lo miramos ahora con un poco más de calma.

[Pajaporte][pajaporte] o bonoporno, son nombres rápidamente acuñados para lo
que el gobierno llama "Cartera Digital", siendo su primer caso de uso,
limitar el acceso a *contenido adulto* (porno, vamos) únicamente a quienes
"puedan demostrar la mayoría de edad en internet".

Luego volvemos a este punto concreto.


## ¡Que alguien piense en les niñes!

Esta estrategia **no es nueva**: a grito de "vamos a proteger a les niñes",
desde frentes como la derecha y la iglesia, siempre nos
intentan colar retiradas de derechos, mecanismos de control estatal y
vigilancia indiscriminada.

En este caso es el gobierno Español del PSOE quien quiere ser "pionero"
en el ámbito.
Este mismo gobierno por cierto, que se posiciona como el más
anticriptografía de toda Europa, siendo el que más se pronunció a favor
del [#ChatControl][chatcontrol], con exactamente la misma estrategia:
¡que alguien piense en les niñes!

[#ChatControl][chatcontrol] es una iniciativa europea, que incluso la
asociación alemana de protección infantil junto con 47 entidades más
de derechos digitales, derechos humanos y derechos de les niñes,
[¡piden que sea retirada!][withdrawchatcontrol]

### El problema real

Además. Hablemos del **problema real**. La única educación sexual que
están recibiendo les niñes es del porno. ¿Y si en vez de intentar quitársela,
fomentamos una **educación sexual** mejor que eso? Así, como idea.

La otra idea es que enchufarle un tablet a un bebé de 2 años para que se calle, tiene
sus consecuencias cuando ya le has dado **acceso a toda internet** y al mundo
adulto en general, no sólo al porno. También a webs nazis (que todos los
gobiernos se olvidan de censurar), vídeos crueles, masacres, publicidad
diseñada para comernos el cerebro, corporaciones que nos comen el alma, y
todos horrores del mundo... así como lo más bello también. El problema en concreto
_no es el porno_.

### Solución real #1: Educación Sexual Integral, de Argentina

Argentina aprobó una ley en 2008 y han desarrollado toneladas de materiales
docentes para fomentar el respeto a la diversidad corporal, de género, hablar
del consentimiento y a la importancia de tomar las propias decisiones.

El Gobierno de Milei lo tiene en el punto de mira, pero aún así aguanta
gracias al movimiento feminista y a iniciativas como el [archivo pirata antifascista][archivo-pirata].

Algunos ejemplos:
- [Yo quiero saber, ¿y vos?](https://esi.pakapaka.gob.ar/)
- [ESI, aprender para decidir](https://www.educ.ar/recursos/158437/esi-aprender-para-decidir)
- [Recursos para docentes](https://www.educ.ar/recursos/151951/educacion-sexual-integral-materiales-para-docentes-y-directi)

[![Logo del proyecto "Yo quiero saber, ¿y vos?"](yoquierosaber.png)](https://esi.pakapaka.gob.ar/)

_Si un enlaces no funciona, copiadlo en la [Máquina del Tiempo](https://web.archive.org/)_

### Solución real #2: Sexólogues, educadores, etc.

Si eres del gremio de los bits, sabrás la rabia que da cuando hacen leyes de mierda
sin ton ni son para prohibir o regular alguna tecnología sin tener ni idea de lo que hablan.

Bien. La gracia es que nosotres nos podemos reír un poco y decir que "ponen puertas al campo",
y encontrar formas de saltarlas, etc. Pero cuando ignoran a la comunidad educativa,
y les coaccionan a seguir el sacrosanto "currículo", no hay vpn que valga.

Durante la dictadura franquista, se montaron **centros de planificación familiar** clandestinos
para repartir anticonceptivos y responder preguntas sobre el sexo. En Estados Unidos, en
todo el contexto fundamentalista cristiano, hay iniciativas como "PlannedParenthood" que
tiene [recursos sobre sexualidad para hijes](https://www.plannedparenthood.org/es/blog/como-hablar-sobre-el-consentimiento-y-las-relaciones-saludables-en-todas-las-edades).

_Compas que estéis en movidas de educación, no nos odiéis por no poner vuestros proyectos,
pasadnos vuestros recursos porfa, que serán mejores seguro, y los añadimos aquí <3
A veces somos mejores encontrando información oculta en internet que hablando con personas_

### Solución real #3: Tutoriales

Buscando un poco, hemos descubierto el fantástico mundo del _eduporn_.
En vez de mil palabras, un ejemplo:

[!["Es importante frotar el clítoris con la pelvis"](pelvis-clitoris.png)](https://es.pornhub.com/playlist/120799821)
_U otro como ["sexo anal"](https://es.pornhub.com/view_video.php?viewkey=ph63542af38c84f) (también para culos de tíos cis-hetero!)_

Que esta persona no es perfecta, habla sólo de sexo hetero-cis con su noviete; no se ve condón
, están delgades-estupendes, etc. Pero aún así hace más que todos
los gobiernos europeos juntos (bofetada de realidad).

_Si sabéis de algunos que cubran más casos, cuerpos y deseos, escribidnos y los añadimos ;)_


### Solución real #4: Porno feminista (o ético, o guay, o tandificilera?)

Dudosamente la "solución" para críes de 8 años, pero a ver, que la ley es hasta los 18.
Encontramos un diagrama que lista proyectos chulos de "porno ético". Es solo un ejemplo!

[![What ethical porn should you consume](ethical-porn.webp)](https://mashable.com/article/how-to-choose-ethical-porn)

También hay docus chulos como [Yes, we fuck](https://www.yeswefuck.org/), que va sobre personas discas
rompiendo todos los preconceptos sexuales posibles y lo explora des del goce sincero.

Si os frustra tener que pagar sin saber antes, preguntad por recomendaciones, o [descargad primero](https://pirateproxylist.net/)
y pagad después, porque... si internet está lleno de porno patriarcal, es
porque les adultes lo consumimos. Y esto nos lleva a:

> _Es que nadie va a pensar en les adultes?_

## Volviendo al Pajaporte mismo

Y bien, ¿qué significa esto del Pajaporte en términos prácticos?

Pues el Pajaporte, o Cartera Digital, es una aplicación móvil (¡en beta!
pero ya será obligatoria, ¡tranquis!) que, en el marco del eIDAS2 (también
todo una movida), ha de estar en medio de todos los trámites con el gobierno:
desde gestionar el padrón, hasta... visitar una página porno
(¿es esto algo del gobierno?).

### ¿A saber si no va de niñes?

Según la [misma Moncloa][pajaporte2]:

> [...] el acceso [al porno] se realiza fundamentalmente a través del teléfono móvil, bien por **mensajería instantánea**, páginas web o **redes sociales**.

El salto es entonces bien rápido: de requerir identificación para acceder a
una página web dedicada a servir pornografía, a incidir en tus chats (hola,
¿[#ChatControl][chatcontrol] otra vez?) y tu uso de redes sociales.


[![Colaboración del resto de agentes en internet Anticipando la adopción de la cartera de identidad que establece el eIDAS2](quierencontrolartodainternetperonolesvamosadejar.png)](010724-cartera-digital-beta.pdf)


Estamos añadiendo "soluciones" tecnocráticas y autoritarias, a problemas
sociales y de educación.


De hecho, vamos a pensar en les niñes realmente.
Y vamos a mirar lo que ha pasado en otras partes del mundo; porque contrario
a lo que dicen, el estado español no es pionero en esto.
[Algunos estados en EEUUA][usporn] y, más cerca, el Reino Unido llevan años
jugando con cosas similares.

[El resultado][ukporn] ha sido, que les usuaries o bien evitan los mecanismos
de los estados de forma trivial, o bien usan otros sitios menos "mainstream" y
con contenido **incluso menos moderado**; un resultado negativo neto.

No sólo eso, sino que efectivamente la extrapolación de estos procesos de
verificación de edad, acabó llegando a redes sociales.


### Necessito l'IDCAT per tramitar l'atur!

¿Os suena que desde hace relativamente poco es obligatorio el certificado electrónico para tramitar el paro ?

No es el único procedimiento, en el Hacklab hemos ayudado a muchas personas
con el proceso de los certificados electrónicos; quien te diga que es un
mecanismo fácil, te engaña.

Las webs del gobierno están mal diseñadas, son hostiles y dificultan que las
personas hagan sus gestiones.

### ¿Qué implica el pajaporte?

Pues al igual que con el paro, para usar el Pajaporte
tendrás que tener un mecanismo con el que te autenticas digitalmente tú,
persona física con nombre XXX XXX y DNI/NIE YYYYYYYYY, delante del gobierno
Español.

En particular, si eres migrante o sólo estás temporalmente, olvídate (`<sarcasmo>`¡que alguien piense en les turistes!`</sarcasmo>`).

[![NWO: Te vigilamos para protegerte (estética perturbadora)](para-protegerte.png)](https://nwocorpsystem.wordpress.com/portfolio/para-protegerte/)

Y esto implica varias cosas:

- Marca una línea muy clara, en la cual el gobierno **da por supuesto** que todo ciudadano está en posesión de un smartphone y por consiguiente, de un móvil con SIM
- Que además, dicho smartphone tiene una aplicación del estado instalada *ehem, [pegasus](https://directa.cat/els-vincles-de-nso-o-com-els-mercats-financers-van-desfermar-pegasus/), ehem*
- Esta aplicación, con casi total seguridad, será de código cerrado; en nombre de la "seguridad informática y de datos", no sabremos qué hace ni cómo, más allá de la promesa que constituye la descripción técnica
- La forma de distribuir estas aplicaciones, será a través de las App Stores centralizadas del duo-polio de sistemas operativos de dispositivos móviles
- Al "ser una aplicación de seguridad", esto implicará que muchos teléfonos relativamente nuevos, pero no de las últimas generaciones, no serán compatibles; así son las políticas de Google
- Esto también seguramente implicará de rebote, que será imposible usar la aplicación en [Android liberado][androidlibre] (sin las mierdas de Google)
- Si no tienes acceso al equivalente a un certificado electrónico estatal, no podrás usarla

Nos dirán que el mecanismo es seguro y privado, lo que no nos dicen
claramente es que todo se basa en una promesa; y ya sabemos lo bien que
cumple sus promesas el gobierno:

> La SGAD, como entidad emisora, no
guardará la vinculación entre la
identidad de los usuarios y las claves
públicas remitidas por los usuarios
desde Cartera Digital

## ¿Es efectivo?

De forma corta y tajante: **NO**.

Creemos que esto es una cortina de humo para seguir con la idea fija de
sometimiento digital de todos los ámbitos de la vida. Empezar con el
problema del porno en la infancia es un **lubricante para lo que vendrá
después**. Por eso es todavía más importante abordar bien el problema.
Como todo lo que no solucionamos el pueblo, viene la derecha y nos cuela
salsas soluciones que sólo benefician al Estado, los ricos, y las corporaciones.

Nosotres rechazamos el tecnosolucionismo, por muy inteligente que pueda
ser la solución a nivel técnico. Humildemente **[hemos propuesto 4 vías
no informáticas](#el-problema-real)** para que las primeras relaciones sexuales de les jóvenes sean más
sanas y respetuosas que las que tuvimos generaciones anteriores.

Respecto a la viabilidad técnica: ni siquiera los gigantes de Streaming
han logrado evitar que la gente use VPNs para **evitar las limitaciones
geográficas** que imponen al contenido por el que la gente paga, y suelen ser
técnicamente más competentes que los gobiernos.

El planteamiento se basa en listas de actores permitidos (le llaman,
de forma *totalmente antiracista* en todas partes: **listas blancas**), y sólo
esos actores permitidos pueden interactuar con el Pajaporte.
Con lo que cada sitio tiene que pasar por algún tipo de proceso de
aprobación y validación, además de implementar la integración con El Gobierno.

Se espera que los proveedores web que no puedan o quieran seguir este proceso,
**serán bloqueados a nivel estatal**.
También es de esperarse que además de los 3 sitios más grandes, los demás
o bien tarden, o bien nunca lo implementen.
Algunos, incluso, podrían simplemente dejar de operar a nivel de red en el
estado Español; como ya hicieron en su día en algunos estados en EEUUA.

Recordemos que [el bloqueo español de Telegram se tuvo que tirar atrás](https://bcn.fedi.cat/notice/AgDrfbw2A8NRjEkqH2)
sin tan sólo implementarlo ante el absurdo técnico que implicaba y la poca
efectividad de la medida.

[![Procede dejar sin efecto la suspensión acordada](pedraz-se-echa-atraz.png)](https://static.ecestaticos.com/file/436/e67/d5d/436e67d5d1d118b0d464c869e5f7b4eb.pdf)

Al igual que con otros bloqueos (o amenazas de bloqueos) históricos,
[hay muchísimas maneras de evitarlo](https://www.digitaldefenders.org/wp-content/uploads/2022/09/2207-ISG_final-es.pdf),
y no sólo por personas adultas que tal vez dispongan de recursos técnicos
o económicos, sino por les niñes mismes.
Podéis ayudar a recoger cosas en este sentido [en el pad de este hilo][lo2].

En lugar de solucionar un problema, sentamos las bases legales y técnicas
para un sistema de vigilancia masiva y control social, se fomenta el
tabú cristiano asociado al sexo, y al final no se gana nada:
todo seguirá estando a tres clicks para todas las personas.

Recordemos también, que los sistemas que implementemos hoy, estarán
disponibles para los gobiernos de mañana. Porque no tienes nada que ocultar
hasta que [tienes que avortar y resulta que Trump es presidente](https://www.washingtonpost.com/technology/2022/07/03/abortion-data-privacy-prosecution/).

Si hace cuatro días nos preocupaba que gobernara la ultra derecha, con esto
les hacemos el trabajo bien fácil para perseguir ideologías o colectivos
como el LGTBIQ+.

> [“There’s quite an incoherent logic that reveals the fact that a lot of policymakers haven’t properly thought this through yet.”][ukporn]

**_Gente, que no nos la metan! Eso ya lo hacemos entre nosostres ;) Salud!_**

[lo1]: https://bcn.fedi.cat/notice/AjUzClA8G5EvzHnc9I
[lo2]: https://bcn.fedi.cat/notice/AjV4qVD3UPpbrfw8CO
[pajaporte]: https://digital.gob.es/especificaciones_tecnicas.html
[pajaporte2]: https://www.lamoncloa.gob.es/serviciosdeprensa/notasprensa/transformacion-digital-y-funcion-publica/Paginas/2024/010724-cartera-digital-beta.aspx
[chatcontrol]: https://bcn.fedi.cat/tag/chatcontrol
[withdrawchatcontrol]: https://edri.org/our-work/joint-statement-on-the-future-of-the-csa-regulation/
[androidlibre]: https://bcn.fedi.cat/notice/AjNqRY3KaLyTDAENmq
[ukporn]: https://www.politico.eu/article/the-great-british-porn-block-is-back/
[usporn]: https://www.nbcnews.com/tech/pornhub-disables-website-texas-rcna143502
[archivopirata]: https://archivo-pirata-antifascista.partidopirata.com.ar/


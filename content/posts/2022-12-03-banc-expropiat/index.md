---
title: "Install party i taller de mòbils!"
date: 2022-12-03
cover: "install-party.jpg"
useRelativeCover: true
tags: ["activitats"]

---

La segona activitat que hem fet al Banc Expropiat del barri de Gràcia ha anat molt bé! Al matí hem començat instaŀlant una distribució de GNU/Linux a un grapat de portàtils. En mòbils, hem instaŀlat [F-Droid](https://fossdroid.com) a uns quants Android, perquè dependre menys de Google i tenir accés més fàcil a aplicacions lliures, que comparat amb les típiques de Google Play, no tenen publicitat ni rastrejadors, són més lleugeres, i no tenen trampes comercials. Fins i tot hem ajudat a una persona amb els seus deures del curs d'alfabetització digital!

Vam aconseguir unes garrafes de kombutxa per oferir alguna beguda sense alcohol refrescant i bona, i repetirem! A la tarda hem explicat alguns conceptes de seguretat en smartphones i s'ha generat una conversa on hem aconseguit respondre a dubtes prou complexos. Vam fer servir el material de la  [presentació sobre els problemes de segurertat dels mòbils](https://sindominio.net/logout/diapos/inseguretat-smartphone)

Ens havíem proposat recollir fons per a donar a Riseup i n'hem arreplegat 90€! Al Logout fem servir [els seus pads](https://pad.riseup.net), i qui més qui menys hi té un compte de correu, fa servir la seva VPN, o participa en alguna llista de correu que allotgen. Així que ens hi sentíem en deute. Podeu veure com contribuir al material i a les despeses de funcionament que tenen a [la seva pàgina de donacions](https://riseup.net/es/about-us/donate).

Fins a la pròxima!
`←] logout`

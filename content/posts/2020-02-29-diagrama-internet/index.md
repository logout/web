---
title: "Diagrama d'internet"
date: 2020-02-29
cover: "img/inet/diagrama-internet.png"
coverCaption: "Dibuix on un mòbil es connecta a través d'una xarxa WiFi i una de dades telefònica, a un servidor a l'altra punta d'internet"
tags: ["logout", "dibuixos", "recursos"]

---

Una xarxa telemàtica és el sistema connectat de dos o més ordinadors, i internet és la interconnexió de moltes d'aquestes xarxes, que poden ser privades de proveïdores d'internet, acadèmiques, estatals, privades de lloguer, comunitàries, privades internes de grans empreses, etc. De fet, els bits que generem són transportats per xarxes de diverses entitats, i no solen travessar-ne més de 6. Finalment, una peça clau per entendre aquestes interconnexions són els **punts neutres** o **punts d'intercanvi**, o IXP. A la zona franca de Barcelona hi tenim el [CATNIX](https://catnix.net), i una de les empreses que ha construït més IXP al món és Equinix.


Altres dibuixos per reutilitzar ([cc-by-sa](https://creativecommons.org/licenses/by-sa/4.0/) 🄯 fadelkon)

{{< gallery match="img/inet/*png" loadJQuery=true resizeOptions="600x600 q90 Lanczos"  >}}

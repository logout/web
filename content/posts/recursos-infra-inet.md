---
title: "Infraestructures d'internet"
date: 202129-03-03
tags: ["logout", "recursos"]
draft: true

---


- Mapes d'infraestructura
  - TeleGeography: Submarine Cable Map - mapa de referència de cables d'internet submarins
  - Infrapedia: Infrapedia app - registre gratuït, té cables submarins i alguns cables de terra
  - SubTel: Cable Map - cables submarins, vaixells de reparació, alguns datacenters.
  - Datacenter Map: centres de dades ; punts neutres
- ShareLabs: The exciting life of an Internet packet

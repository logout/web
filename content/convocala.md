---
title: "Convoca-la BCN"
cover: "https://bcn.convoca.la/logo.png"
description: "Activitats de convoca-la bcn i més enllà!"

---

## BCN convoca-la!

A [bcn.convoca.la](https://bcn.convoca.la/about) diuen:
> bcn.convoca.la som un coŀlectiu de militants i activistes que impulsem i mantenim una agenda tecnològicament i políticament autònoma, que vol contribuir a difondre i coordinar accions públiques de coŀlectius socials i polítics afins.

... seguint l'estela d'[infousurpa](https://usurpa.info/), [indymedia](http://bcn.indymedia.org) i [radar.squat.net](https://radar.squat.net/ca/events/country/XC/city/barcelona/country/XC) &lt;3

Fa servir [gancio](https://gancio.org) i està allotjat a [sindominio](https://sindominio.net/ayuda/#agenda-comunitaria)

## Activitats a Can Vies
<gancio-events baseurl="https://bcn.convoca.la" title="CSA Can Vies (amb recorrents)" maxlength=5 places="2" show_recurrent="true" sidebar="true" theme="dark"></gancio-events>

## Convocatòries a BCN
<gancio-events baseurl="https://bcn.convoca.la" title="Tota Barcelona"  show_recurrent="false" sidebar="false" theme="dark"></gancio-events>

<!--script src="https://bcn.convoca.la/gancio-events.es.js"></script-->
<script src="/js/gancio-events.es.js"></script>

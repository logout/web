---
title: "Recursos interessants i pàgines amigues"
cover: "img/amigues.jpg"
description: "Llista de recursos interessants i pàgines amigues, sempre en construcció"
toc: true
tags: ["articles", "recursos", "amigues"]
---

# Recursos interessants i pàgines amigues

En aquesta secció anirem afegint recursos interessants i pàgines amigues que ens semblin útils o divertides. Si tens 
alguna proposta, no dubtis a contactar-nos!

Subdividirem els recursos en diferents categories, però no et preocupis si no trobes la que busques, ja que aquesta
secció està en construcció i anirem afegint-ne més.

## Autodefensa digital feminista

Vols aprendre a protegir-te de les violències masclistes digitals? Vols saber com protegir la teva privacitat a la xarxa?
Aquí tens algunes pàgines que et poden ajudar:

### Col·lectives amiguis

- [Fembloc](https://fembloc.cat/): FemBloc és una iniciativa feminista sense ànim de lucre per a la prevenció, detecció, atenció i reparació de les Violències masclistes digitals (VMD).
- [Donestech](https://donestech.net/): Donestech és una comunitat de dones que treballen en tecnologia i que volen fomentar la presència de les dones en el món de la tecnologia.

### Recursos

Recursos o índex de recursos. Aquí tens algunes pàgines que et poden ajudar sobre diferentes temàtiques de ciberfeminisme:

- [acoso.online](https://acoso.online/): orientación a víctimas de 19 países de América Latina y El Caribe, además de España. Encontrarás orientación legal, judicial y comunitaria, además de qué hacer en las plataformas más populares cuando se enfrenta un caso. 
- [autodefensa.online](https://autodefensa.online/): Red autodefensa feminista online. **Tenen una gran llistade recursos a veure!** https://autodefensa.online/recursos.html?cerrar
- [Ciberseguras](https://ciberseguras.org/): Ciberseguras es un proyecto de la Asociación de Internautas que tiene como objetivo la formación y concienciación en ciberseguridad de las mujeres.
- [Desconecta de tu expareja](https://desconectadetuex.net/): Guía para la desconexión digital de tu expareja. Herramientas y recursos para protegerte de la violencia de género digital.
- [Quiz de #SextingSeguro](https://segudigital.org/sexting/): #Sextear es chido! Oh sí... Es súper rico... compartir textos, fotos y videos... Con una persona o con muchas 😉 pero, esos nudes que tanto gozas... ¿Qué tan bien los cuidas?
- [Test de seguridad digital](https://fortuna.segudigital.org/es): ¡Que Mercurio retrógrado no afecte tus comunicaciones digitales! Contesta este test y descubre cuidados que puedes incorporar en las acciones de comunicación en línea de tu organización, medio o colectivx.

## Migrar a software lliure

Vols buscar alternatives a les aplicacions privatives que utilitzes a diari? Vols sortir de la gàbia de les grans corporacions? 

Aquí tens algunes pàgines que et poden ajudar:

- [Prism-Break](https://prism-break.org/): Prism-Break és un lloc web que recull alternatives de programari lliure per a les aplicacions més comunes que utilitzem a diari.
- [Switching.software](https://switching.software/): Switching.software és un lloc web que recull alternatives de programari lliure.
- [Privacy guides](https://www.privacyguides.org/en/): Privacy Guides és un lloc web que recull guies de privacitat i alternatives per a les aplicacions més comunes que utilitzem a diari.
- [Alternativeto](https://alternativeto.net/): Alternativeto és un lloc web que recull alternatives de programari lliure.

## Canviar de xarxes socials

Vols sortir de les xarxes socials privatives i centralitzades? Vols trobar alternatives més respectuoses amb la teva privacitat?

- [La furgo](https://sindominio.net/lafurgo/): La Furgo és una campanya per facilitar un trasllat organitzat des de les xarxes socials comercials a les autogestionades.
- [Fediverse.party](https://fediverse.party/): Fediverse.party és un lloc web que recull alternatives de xarxes socials descentralitzades i lliures.
- [Join Fediverse](https://jointhefediverse.net/) és un lloc web que recull alternatives de xarxes socials descentralitzades i lliures.

### Instancies amiguis al fedivers

El fedivers és una xarxa social descentralitzada que permet la interconnexió entre diferents xarxes socials lliures. 
Per fer-te una conta al fedivers, pots triar una instància que et convingui més. Una instància és un servidor que allotja
una xarxa social lliure.

Hi ha molts exemples de xarxes socials lliures al fedivers. Busca la que més t'agradi i comença a compartir!

A continuació, et deixem algunes instàncies amigues:

- [fedi.cat](fedi.cat): punt de trobada i de debat del fedivers català
- [bcn.fedi.cat](bcn.fedi.cat): node akkoma gestionat col·lectivament.
- [Kolektiva.social](Kolektiva.social): node mastodon anarquista, gestionat entra Canadà (submedia) i Brasil (antimidia)
- [Anartist.org](Anartist.org): punt de trobada d'artistes, sobretot en música, que mutualitzen un mastodon, un fòrum, una discogràfica (radi solar) i més

## Seguretat i privacitat

### Thread models

Els "thread models" són models de seguretat que et permeten identificar les amenaces a les quals estàs exposat i com.
Això et permet dissenyar un pla de seguretat personalitzat per protegir-te de la vigilància.

- [Security in a box](https://securityinabox.org/en/): recurs per ajudar a la gent en risc a protegir la seva seguretat i privacitat digital.
- [Threat modeling for campaigners](https://mobilisationlab.org/stories/threat-modeling-for-campaigners-and-activists/): 
article que descriu un model pensat per a activistes per evaluar els riscs online.
- [Suggested Use of the Threat Library](https://www.notrace.how/threat-library/tutorial.html): tutorial per utilitzar la biblioteca d'amenaces i crear un thread model.
- [The Protesters' Guide to Smartphone Security](https://www.privacyguides.org/articles/2025/01/23/activists-guide-securing-your-smartphone/): guia bàsica per protegir el teu smartphone en una protesta.
- [Digital Security Checklists for Activists](https://activistchecklist.org/): llista de comprovacions per protegir la teva seguretat digital.
- [Tech Guides for Anarchists](https://www.anarsec.guide/): guies de seguretat digital per a anarquistes.

### Eff

L'Electronic Frontier Foundation (EFF) és una organització sense ànim de lucre que defensa els drets digitals que ha 
desenvolupat molts recursos per protegir-se de la vigilància massiva. Pots trobar-los a la seva pàgina web a l'apartat 
[d'eines](https://www.eff.org/pages/tools) però aquí et deixem alguns recursos interessants:

- [Atlas de la vigilancia](https://atlasofsurveillance.org/): es una base de dades on es documenta la vigilància a nivell de carrer a tots els Estats Units.
- [Al anar a una protesta](https://ssd.eff.org/module/attending-protest): recurs en anglès de l'Electronic Frontier Foundation (EFF) per protegir-se de la vigilància massiva en una protesta.
- [Empremta digital del navegador](https://coveryourtracks.eff.org/): el navegador deixa una empremta digital que pot ser utilitzada per identificar-te. Aquest recurs de l'Electronic Frontier Foundation (EFF) et permet comprovar la teva petjada digital i com millorar-la.
- [Vigilancia a nivell de carrer](https://sls.eff.org/es/): recull de diferentes tecnologies a nivell de carrer. 

### Atlas d'abusos

Recopilació de casos on s'han utilitzat tecnologies de vigilància per reprimir moviments socials:

- [Biblioteca d'amenaces](https://www.notrace.how/threat-library/): base de coneixement de les tècniques repressives utilitzades pels enemics dels anarquistes i altres rebels i les operacions repressives on s'han utilitzat.
- [Ears and Eyes](https://www.notrace.how/earsandeyes/): Base de dades cercable de casos de dispositius de vigilància física (micròfons, càmeres, rastrejadors d'ubicació...) ocults per les forces de l'ordre i les agències d'intel·ligència per vigilar persones o grups que participen en activitats subversives
- [Antifa Watch](https://antifawatch.net/): pàgina on neonazis dels estats units publiquen informació sobre antifeixistes que han recopilat a internet. Per a tenir en compte la importancia de protegir la nostra privacitat

## Combatre la desinformació

Recopies de recursos per combatre la desinformació:

- [#FakeYou](https://xnet-x.net/es/fakeyou-desinformacion-descarga/): guia de Xnet per combatre la desinformació
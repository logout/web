## WEB LOGOUT HACKLAB
Aquí se aloja el sitio Hugo que genera el sitio estático que se publica en tu espacio web.

Un sitio de hugo es un árbol de directorios con ficheros de contenido (con sintáxis markdown, no hay base de datos), imágenes, css, js, plantillas (escritas en go), configuración...  

Cuando quieras actualizar el sitio, debes hacerlo en https://0xacab.org/logout/web.

Este tiene un mirrir en https://git.sindominio.net/logout/web, y es desde alli
que se dispara un webhook, que ejecuta el script de actualizacion del
contenido


## CONTENIDO

En nuestro sitio hugo (repositorio) tenemos una carpeta `content/` donde crearemos nuestros ficheros .md, que contienen nuestros textos para el contenido.  


### TEMA
En hugo, un tema incluye una diagramacion del contenido, y tambien una serie
de 'shortcodes' que agregan funcionalidades al formato markdown propio de
hugo.

En este sitio utilizamos el tema [hello-friend](https://github.com/panr/hugo-theme-hello-friend), y recomendamos que tambien leas
su documentacion



### CREAR CONTENIDO
Encabezando los ficheros tenemos el 'frontmatter', un bloque donde podemos definir algunos metadatos del contenido. Si bien no es necesario que exista ningún campo, conviene definir como mínimo el título. Si nuestro sitio web va a mostrar fechas deberemos definir también la fecha.

Si quieres crear contenido desde gitea, crea un nuevo fichero con extensión `.md`. Añade la cabecera (frontmatter) y escribe tu artículo.
```
---
title: Mi artículo
date: 16 may, 2020
---
Quiero empezar mi artículo señalando cómo se escribe en *cursiva*
...
..
.
```

Otra opción sería escribir el artículo dentro una carpeta llamada *miarticulo* que contenga un fichero *_index.md* o *index.md*, dependiendo del tema que estés usando.

> El tema que se usa por defecto en los repos desplegados desde lowry es [mainroad](https://github.com/Vimux/mainroad) (desde junio 2020, antes usabamos [minimal_bootstrap](https://github.com/zwbetz-gh/minimal-bootstrap-hugo-theme), que era más sencillo y probablemente demasiado simple).
> Este tema permite usar un campo *thumbnail* en el frontmatter para incluir una imágen como miniatura e imágen destacada (para la vista de lista de artículos, y artículo). La imagen tendrá que estar dentro de la carpeta static del repo. Por ej, si la subes a *static/img/*, en el frontmatter añadirás: `thumbnail: "img/mi_imagen.png"`



El texto que escribas usa sintáxis markdown, de forma que puedes añadir recursos estilíticos para salir del texto plano. Tienes un manual de introducción a markdown en https://sindominio.net/trastienda/manuales/markdown_intro/. Puedes insertar imágenes, ficheros, títulos, enlaces, tablas ...

Al guardar el contenido se publicará en la web. 

### AÑADIR IMÁGENES
Hugo permite que las imágenes (y ficheros) que queremos añadir al blog estén en cualquier lugar de nuestra carpeta content. En función de dónde subamos los ficheros tendremos que construir los enlaces a los mismos.

Por otro lado hugo permite crear una página creando un fichero con extensión `.md`, pero también creando una carpeta que contenga un fichero `_index.md` (`index.md,  en algunos temas`).

Por lo tanto, si vamos a crear un artículo con sus imágenes, puede ser conveniente que creemos éste como carpeta, con el texto en su `_index.md` y las imágenes al lado en la carpeta, o incluso en una subcarpeta llamada img/ o images/. Al gusto. 

Supongamos que creamos un artículo "Bye bye Marte". Creamos una carpeta llamada `byebye_marte` y en ella un fichero `index.md`. 

>Nota: En gitea (web) creas una carpeta al crear un archivo y poner como nombre `nombre_carpeta/nombre_fichero.md`, es decir, insertando el símbolo `/` entre carpeta y fichero.
```
---
title: Bye bye Marte
date: 16 May, 2020
---
La foto del planeta rojo:

```
Ahora (o antes) subimos la imagen a la carpeta recién creada, y añadimos al texto del fichero:
```
![Texto Alternativo](nombre_del_fichero.png)

```
Si la imagen estuviese en una subcarpeta `img` haríamos:
```
![Texto Alternativo](img/nombre_del_fichero.png)

```
Y si está en una carpeta anterior en el árbol del repo:
```
![Texto Alternativo](../nombre_del_fichero.png)

```
Si necesitas más flexibilidad puedes usar directamente código html en tu artículo:
```
<img  class="" src="mycats" width="300" height="200" alt="Gatitos">
```
Y escribir una clase css para configurar más finamente tu imagen.


### AÑADIR ETIQUETAS

Hugo soporta taxonomías de forma nativa: etiquetas y categorías. Basta con que añadas la etiqueta en la cabecera de tu fichero de contenido:
```
title: Mi contenido
date: 19 Mayo, 2020
tags: ["ciencia", "política"]
categories: ["lectura"]
```

### ESTRUCTURA DEL CONTENIDO

Dentro de `content/` podemos organizar el contenido en carpetas con el nombre que queramos. Formarán parte de la url en la que se accede a los contenidos de la carpeta. Por ejemplo un fichero `content/priv/hola_marte.md` se accederá en https://sindominio.net/priv/hola_marte.  

Estas carpetas serán nuestras secciones. Si en la raíz de cada sección creamos un fichero _index.md, dicha sección tendrá su página de entrada propia, desde donde podemos enlazar el resto de páginas.



### TRABAJANDO EN LA TERMINAL 
Hugo genera a partir de un sitio hugo (nuestro árbol de directorios del repo) el contenido web estático. Dado que nuestro sitio web está en un repositorio git lo primero que haremos para empezar a trabajar será clonarlo a nuestra máquina local.
```
git clone https://git.sindominio.net/miusuaria/mirepoweb.git
```

Entramos en la carpeta clonada. Puedes crear un nuevo artículo desde la subcarpeta de content donde vayas a añadirlo:
```
hugo new hola_mundo.md
```
Hugo creará el fichero y escribirá una cabecera (frontmatter) básica con el título, la fecha, y el campo 'draft' como true; creará un borrador. Tendremos que cambiar el campo a false para que se incorpore al resultado estático. Edita el tu artículo bajo el frontmatter y guarda los cambios.

#### Previsualización
Cuando hayas hecho algún cambio querrás previsualizarlo. Para ello puedes construir la web en tu máquina.
```
  hugo -s ruta_al_árbol -d ~/static_ouput_dir -b baseURL'
```

`-s` source, nuestro sitio hugo.  
`-d` destination, salida html más assets (img, css, js, ...); lo que se publica.
`-b` baseURL, el dominio (y en su caso carpeta) que se usará para escribir las direcciones de los assets.  

Por ejemplo:
```
hugo -s ~/hugo/mi_sitio -d ~/carpeta_destino -b localhost
```
Esto escupirá el resultado en tu carpeta destino (-d). 
Pero si estás haciendo pruebas en tu máquina puedes previsualizar los cambios que vas haciendo en los ficheros con:
```
    hugo -D --bind localhost -p 1313 server -b http://localhost/
```
Verás la web con tu navegador en http://localhost:1313/.

> Nota: debes definir un puerto, dado que de lo contrario usaríamos el 80 (default http port), y salvo que seas root hugo usará otro (1313 posiblemente)

De este modo cualquier modificación de un fichero en el repo, por ejemplo guardar un artículo en el que estás trabajando, actualizará la web en tu navegador sin que ni siquiera tengas que refrescar la página.
<br>

#### Haciendo commit de los cambios
Tocamos el tema, añadimos nuevas páginas, las enlazamos en otras, creamos una nueva sección de contenido,  añadimos nuestro media ...

Y cuando creamos que hemos avanzado lo suficiente como para actualizar el sitio usamos comandos git para subir el contenido.
```
git add --all
git commit -S -m 'Comentario a la actualización'
git push
```

> **Atención:** `-S` firma los commit, las subidas que hacemos al repo. Para hacerlo tenemos que tener una clave gpg en nuestra máquina y haber subido nuestra parte pública a nuestro perfil en gitea. Si no tienes, es tiempo de tenerla. Elimina '-S' si no vas a firmar.


</br>

### AVANZADO: Modficar el tema - CSS y plantillas.

La carpeta `static/` alberga las imágenes, estilos y demás ficheros que se servirán en nuestra web. Podemos crear carpetas para ordenar estos ficheros, como `css/` o `img/`. El html resultante, por medio de las plantillas, podrá incluir estos recursos como https://sindominio/css/style.css y http://sindominio.net/img/myimage.png.

Así, en la carpeta css podemos sobreescribir el css del tema.

Las plantillas del tema las sobreescribimos en la carpeta `layouts/`. Allí replicamos la estructura de carpetas necesaria para copiar la plantilla a modificar y modificarla.

Para crear una página principal (y no la lista de artículos) puedes crear los ficheros `/layout/index.html` y `content/_index.md`, plantilla y contenido respectivamente. Con ellos hugo construirá un index.html en la raíz del resultado, y por tanto será tu página de llegada. Allí podrás enlazar a otras páginas y secciones.


</br>


---

**CONTENIDO RELACIONADO:**

* [Webs estáticas con git](https://sindominio.net/trastienda/manuales/web_git/)
* [Markdown](https://sindominio.net/trastienda/manuales/markdown_intro/)
* [Cómo conectar a mi home (ssh, sftp)](https://sindominio.net/trastienda/manuales/conectar_home/)  

---  
Si necesitas ayuda puedes encontrarnos en:  

* <mail@sindominio.net> , nuestro correo de soporte.  
* ['La colmena'](https://chat.sindominio.net/#/room/#lacolmena:sindominio.net), nuestra sala en matrix.  


